// Homework 19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

using namespace std;

class Animal
{
protected:
	string voice;
public:
	Animal() {};
	Animal(string _voice) :voice(_voice) {
		cout << voice<<endl;
	}
	
		
	

private:

};

class Dog :Animal
{
protected:
	string voice;

public:
	Dog() {};
	Dog(string _voice) :voice(_voice) {
		cout << voice << endl;
	}
	
		
	

private:

};

class Cat :Animal {
protected:
	string voice;

public:
	Cat() {};
	Cat(string _voice) :voice(_voice) {
		cout << voice << endl;
	}
	




};

class Bird :Animal {
protected:
	string voice;

public:
	Bird() {};
	Bird(string _voice) :voice(_voice) {
		cout << voice << endl;
	}
	



};

class Fox :Animal {
protected:
	string voice;
public:
	Fox() {};
	Fox(string _voice) :voice(_voice) {
		cout << voice << endl;
	}
	
};


int main()
{
	setlocale(LC_ALL, "ru");
	Cat *cat = new Cat();
	Dog *dog = new Dog();
	Bird *bird = new Bird();;
	Fox *fox = new Fox();
	Animal* animal[3];

	animal[0] = &cat;
	animal[1] = &dog;
	animal[2] = &bird;
	animal[3] = &fox;

}
	

